<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Traits\Methods;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller {
   use ApiResponser;
   use Methods;
   public function list(Request $request) {
      try {
         //recuperamos la variable de busqueda
         $search = $request->search;
         //listamos las preioridades que no tengan contratos y que esten activos segun el rol
         $usuarios = DB::table('students')
            ->select('students.id', 'students.firstName', 'students.lastName', 'students.created_at', 'students.mobileNumber')
            ->where('students.deleted_at', '=', null)
            ->orderBy('students.created_at', 'desc')
            ->where(function ($query) use ($search) {
               $query->orWhereRaw('LOWER(students.firstName) like ?', "%" . strtolower($search) . "%");
               $query->orWhereRaw('LOWER(students.lastName) like ?', "%" . strtolower($search) . "%");
            })
            ->get();
         //mostramos una respuesta en json
         $message = 'Lista recuperada correctamente.';
         return $this->SuccessResponse($usuarios, $message);
      } catch (\Throwable $error) {
         $message = 'Obteción de datos fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_INTERNAL_SERVER_ERROR);
      }
   }

   public function show($id) {
      try {
         //listamos las preioridades que no tengan contratos y que esten activos segun el rol
         $usuarios = DB::table('students')
            ->where('students.id', '=', $id)
            ->first();
         $usuarios->password = Crypt::decryptString($usuarios->password);
         //mostramos una respuesta en json
         $message = 'Datos recuperados correctamente.';
         return $this->SuccessResponse($usuarios, $message);
      } catch (\Throwable $error) {
         $message = 'Obteción de datos fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_INTERNAL_SERVER_ERROR);
      }
   }

   public function create(Request $request) {
      try {
         $validator = Validator::make($request->all(), [
            'firstName'    => 'required',
            'lastName'     => 'required',
            'email'        => 'required',
            'password'     => 'required',
            'mobileNumber' => 'required',
         ]);

         if ($validator->fails()) {
            return $this->ErrorResponse("Validacion de datos fallida.", $validator->errors(), Response::HTTP_BAD_REQUEST);
         }
         //CREAMOS UN ARRAY CON LAS COLUMNAS A REGISTRAR
         $column = array(
            'id'           => $this->generateUUID(),
            'firstName'    => $request->firstName,
            'lastName'     => $request->lastName,
            'email'        => $request->email,
            'password'     => Crypt::encryptString($request->password),
            'mobileNumber' => $request->mobileNumber,
            'created_at'   => date('Y-m-d H:i:s', time()),
            'updated_at'   => date('Y-m-d H:i:s', time()),
         );
         DB::table('students')->insert($column);

         $message = 'Registro creado.';
         return $this->SuccessResponse($column, $message, Response::HTTP_CREATED);
      } catch (\Throwable $error) {
         $message = 'Creacion fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_NOT_FOUND);
      }
   }

   public function update(Request $request, $id) {
      try {
         $validator = Validator::make($request->all(), [
            'firstName'    => 'required',
            'lastName'     => 'required',
            'email'        => 'required',
            'password'     => 'required',
            'mobileNumber' => 'required',
         ]);

         if ($validator->fails()) {
            return $this->ErrorResponse("Validacion de datos fallida.", $validator->errors(), Response::HTTP_BAD_REQUEST);
         }
         //CREAMOS UN ARRAY CON LAS COLUMNAS A REGISTRAR
         $column = array(
            'firstName'    => $request->firstName,
            'lastName'     => $request->lastName,
            'email'        => $request->email,
            'password'     => Crypt::encryptString($request->password),
            'mobileNumber' => $request->mobileNumber,
            'updated_at'   => date('Y-m-d H:i:s', time()),
         );
         DB::table('students')
            ->where('id', $id)
            ->update($column);

         $message = 'Registro actualizado.';
         return $this->SuccessResponse($column, $message, Response::HTTP_CREATED);
      } catch (\Throwable $error) {
         $message = 'Actualizacion fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_NOT_FOUND);
      }
   }

   public function delete($id) {
      try {
         //CREAMOS UN ARRAY CON LAS COLUMNAS A REGISTRAR
         $column = array(
            'deleted_at' => date('Y-m-d H:i:s', time()),
         );
         DB::table('students')
            ->where('id', $id)
            ->update($column);

         $message = 'Registro eliminado.';
         return $this->SuccessResponse($column, $message, Response::HTTP_NO_CONTENT);
      } catch (\Throwable $error) {
         $message = 'Actualizacion fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_NOT_FOUND);
      }
   }
}
