<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use App\Traits\Methods;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller {
   use ApiResponser;
   use Methods;
   public function list(Request $request, $studentId) {
      try {
         //recuperamos la variable de busqueda
         $search = $request->search;
         //listamos las preioridades que no tengan contratos y que esten activos segun el rol
         $usuarios = DB::table('courses')
            ->where('courses.deleted_at', '=', null)
            ->where('courses.studentId', '=', $studentId)
            ->orderBy('courses.created_at', 'desc')
            ->where(function ($query) use ($search) {
               $query->orWhereRaw('LOWER(courses.name) like ?', "%" . strtolower($search) . "%");
               $query->orWhereRaw('LOWER(courses.description) like ?', "%" . strtolower($search) . "%");
            })
            ->get();
         //mostramos una respuesta en json
         $message = 'Lista recuperada correctamente.';
         return $this->SuccessResponse($usuarios, $message);
      } catch (\Throwable $error) {
         $message = 'Obteción de datos fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_INTERNAL_SERVER_ERROR);
      }
   }

   public function create(Request $request, $studentId) {
      try {
         $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'description' => 'required',
         ]);

         if ($validator->fails()) {
            return $this->ErrorResponse("Validacion de datos fallida.", $validator->errors(), Response::HTTP_BAD_REQUEST);
         }
         //CREAMOS UN ARRAY CON LAS COLUMNAS A REGISTRAR
         $column = array(
            'id'          => $this->generateUUID(),
            'studentId'   => $studentId,
            'name'        => $request->name,
            'description' => $request->description,
            'created_at'  => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
         );
         DB::table('courses')->insert($column);

         $message = 'Registro creado.';
         return $this->SuccessResponse($column, $message, Response::HTTP_CREATED);
      } catch (\Throwable $error) {
         $message = 'Creacion fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_NOT_FOUND);
      }
   }

   public function delete($id) {
      try {
         //CREAMOS UN ARRAY CON LAS COLUMNAS A REGISTRAR
         $column = array(
            'deleted_at' => date('Y-m-d H:i:s', time()),
         );
         DB::table('courses')
            ->where('id', $id)
            ->update($column);

         $message = 'Registro eliminado.';
         return $this->SuccessResponse($column, $message, Response::HTTP_NO_CONTENT);
      } catch (\Throwable $error) {
         $message = 'Actualizacion fallida.';
         return $this->ErrorResponse($message, $error, Response::HTTP_NOT_FOUND);
      }
   }
}
