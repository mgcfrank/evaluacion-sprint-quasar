<?php

namespace App\Traits;

use DateTime;

trait Methods {

   public function generateTokenRandom($longitud) {
      if ($longitud < 4) {$longitud = 4;}
      return bin2hex(random_bytes(($longitud - ($longitud % 2)) / 2));
   }
   public function generateMicrotimeDate() {
      $now = DateTime::createFromFormat('U.u', microtime(true));
      return $now->format("YmdHisu");
   }

   public function generateUUID() {
      return $this->generateMicrotimeDate() . $this->generateTokenRandom(5);
   }
}
