<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser {

   public function SuccessResponse($data, $message = null, $code = Response::HTTP_OK) {
      $response = [
         'success' => true,
         'code'    => $code,
         'message' => $message,
         'data'    => $data,
      ];

      return response()->json($response, $code);
   }

   public function ErrorResponse($message = null, $errorMessages = [], $code) {
      $response = [
         'success' => false,
         'code'    => $code,
         'message' => $message,
         'data'    => $errorMessages,
      ];

      return response()->json($response, $code);
   }
}
