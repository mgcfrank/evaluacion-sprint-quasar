<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('students', [StudentController::class, 'list']);
Route::get('students/{id}', [StudentController::class, 'show']);
Route::post('students', [StudentController::class, 'create']);
Route::put('students/{id}', [StudentController::class, 'update']);
Route::delete('students/{id}', [StudentController::class, 'delete']);
Route::get('student/{id}/courses', [CourseController::class, 'list']);
Route::post('student/{id}/courses', [CourseController::class, 'create']);
Route::delete('student/courses/{id}', [CourseController::class, 'delete']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
   return $request->user();
});
