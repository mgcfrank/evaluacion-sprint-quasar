<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {
   /**
    * Seed the application's database.
    *
    * @return void
    */
   public function run() {
      $columna = array(
         'id'           => '202101211716262bae8d',
         'firstName'    => 'Alejandro',
         'lastName'     => 'Martinez',
         'email'        => 'alejandro@gmail.om',
         'password'     => Crypt::encryptString('qwerty'),
         'mobileNumber' => '591-70190123',
         'created_at'   => date('Y-m-d H:i:s', time()),
         'updated_at'   => date('Y-m-d H:i:s', time()),
      );
      DB::table('students')->insert($columna);

      $columna = array(
         'id'           => '202101211716265bae6f',
         'firstName'    => 'Marisol',
         'lastName'     => 'Gomez',
         'email'        => 'marisolg@gmail.om',
         'password'     => Crypt::encryptString('qwerty'),
         'mobileNumber' => '591-73190523',
         'created_at'   => date('Y-m-d H:i:s', time()),
         'updated_at'   => date('Y-m-d H:i:s', time()),
      );
      DB::table('students')->insert($columna);

      $columna = array(
         'id'          => '202101211716262bae8d',
         'studentId'   => '202101211716262bae8d',
         'name'        => 'English Module - 1',
         'description' => 'Sin descripcion',
         'created_at'  => date('Y-m-d H:i:s', time()),
         'updated_at'  => date('Y-m-d H:i:s', time()),
      );
      DB::table('courses')->insert($columna);

      // \App\Models\User::factory(10)->create();
   }
}
