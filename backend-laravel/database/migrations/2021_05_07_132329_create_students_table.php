<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration {
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up() {
      Schema::create('students', function (Blueprint $table) {
         $table->string('id', 35)->primary()->unique();
         $table->string('firstName', 50);
         $table->string('lastName', 50);
         $table->string('email', 60);
         $table->string('password', 250);
         $table->string('mobileNumber', 15);
         $table->dateTime('created_at');
         $table->dateTime('updated_at');
         $table->dateTime('deleted_at')->nullable(true);
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down() {
      Schema::dropIfExists('students');
   }
}
