<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration {
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up() {
      Schema::create('courses', function (Blueprint $table) {
         $table->string('id', 35)->primary()->unique();
         $table->string('name', 120);
         $table->string('description', 520);
         $table->string('studentId', 35);
         $table->foreign('studentId')->references('id')->on('students');
         $table->dateTime('created_at');
         $table->dateTime('updated_at');
         $table->dateTime('deleted_at')->nullable(true);
      });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down() {
      Schema::dropIfExists('courses');
   }
}
