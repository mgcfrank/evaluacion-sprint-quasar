// import Vue from 'vue'
import axios from 'axios'
//configuramos la url base del api
const baseURL = 'http://127.0.0.1:8000/api';

let apiJson = axios.create({
  baseURL: baseURL,
  headers: {
    'Authorization': "Bearer " + localStorage.getItem("access_token"),
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept'
  }
});
// response interceptor
apiJson.interceptors.response.use(response => {
  //envia respuestas recibidas
  console.log(response);
  return response
}, err => {
  console.log(err);
  if (err.response) {
    //verificamos que el token no haya caducado
    if (err.response.status == 401) {
    }
  }else if (err.request) {
      // client never received a response, or request never left
      alert("No hay respuesta del servidor: \n\n" + err.request);
    } else {
      // anything else
      alert("No hay conexion con el servidor: \n\n" + err);
    }
  //envia respuestas fallidas 500 o 400
  return Promise.reject(err);
});

function sendForm(method, url, dataForm = {},) {
  return apiJson({
    url: url,
    method: method,
    data: dataForm,
    headers: {
      // 'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Type': 'multipart/form-data'
    }
  });
}

function send(method, url, dataForm = {},) {
  return apiJson({
    url: url,
    method: method,
    data: dataForm,
    headers: {
      'Content-Type':'application/json',
    }
  });
}


export default {
  refresh() {
    apiJson = axios.create({
      baseURL: baseURL,
      headers: {
        'Authorization': "Bearer " + localStorage.getItem("access_token"),
      }
    });
  },
  //END POINTS ESTUDIANTES
  getStudent({search=""}) {
    return send("GET","/students?search=" + search);
  },
  getStudentDetails({studentId}) {
    return send("GET","/students/" + studentId);
  },
  createStudent({ form }) {
    return sendForm("POST", "/students", form)
  },
  updateStudent({ body_raw, studentId }) { return send("PUT", "/students/"+studentId, body_raw) },
  deleteStudent({ studentId }) { return send("DELETE", "/students/" + studentId) },

  //END POINTS CURSOS POR ESTUDIANTE
  getStudentCourses({studentId, search=""}) {
    return send("GET","/student/"+studentId+"/courses?search=" + search);
  },
  createStudentCourse({ form, studentId }) {
    return sendForm("POST", "/student/" + studentId + "/courses", form)
  },
  deleteStudentCourse({ courseId }) { return send("DELETE", "/student/courses/" + courseId) },

}
// Vue.prototype.$axios = axios
