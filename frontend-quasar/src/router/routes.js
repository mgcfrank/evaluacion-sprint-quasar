const routes = [
  {
    path: '/',
    name: "student", component: () => import('layouts/student/Home.vue'),
  },
  {
    path: '/student/create',
    name: "student-create", component: () => import('layouts/student/Create.vue'),
  },
  {
    path: '/student/update/:studentId',
    name: "student-update", component: () => import('layouts/student/Update.vue'),
  },
  {
    path: '/courses/:studentId',
    name: "courses", component: () => import('layouts/course/Home.vue'),
  },
  {
    path: '/courses/create/:studentId',
    name: "courses-create", component: () => import('layouts/course/Create.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
